<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ReportingFileRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ReportingFileRepository::class)]
#[ApiResource(
    paginationClientItemsPerPage: true,
    normalizationContext: ['groups' => ['get:ReportingFile']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
)
]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'file_path' => "partial",'user'=>"partial"])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at'])]
class ReportingFile
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:ReportingFile'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['get:ReportingFile'])]
    private $file_path;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:ReportingFile'])]
    private $created_at;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:ReportingFile'])]
    private $updated_at;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'reportingFiles')]
    #[ORM\JoinColumn(name: "user_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:ReportingFile'])]
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFilePath(): ?string
    {
        return $this->file_path;
    }

    public function setFilePath(?string $file_path): self
    {
        $this->file_path = $file_path;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
