<?php

namespace App\Entity;

use ApiPlatform\Core\Action\NotFoundAction;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\MeController;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get',
        'post',
        'me' => [
            'pagination_enabled' => false,
            'path' => '/me',
            'method' => 'get',
            'controller' => MeController::class,
            'read' => false,
        ]
    ],
    // security: 'is_granted("ROLE_USER")',
    itemOperations: [
        'put',
        'patch',
        'get' => [
            'controller' => NotFoundAction::class,
            'openapi_context' => ['summary' => 'hidden'],
            'read' => false,
            'output' => false
        ]
    ],
    normalizationContext: ['groups' => ['get:User']],
    paginationClientItemsPerPage: true,
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
)]
#[UniqueEntity('email','username')]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact","email" => "partial","last_name"=>"partial","first_name"=>"partial","roles['0']"=>"exact"])]
#[ApiFilter(BooleanFilter::class, properties: ['is_active'=>"exact", "confirmed_account"=>"exact"])]
#[ORM\EntityListeners(['App\EntityListener\UserListener'])]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:User','get:ElementHistory'])]
    private $id;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    #[Groups(['get:User'])]
    private $email;

    #[ORM\Column(type: 'json')]
    #[Groups(['get:User'])]
    private $roles = [];

    #[ORM\Column(type: 'string')]
    private $password;

    #[ORM\ManyToOne(targetEntity: Profile::class, inversedBy: 'users')]
    #[ORM\JoinColumn(name: "profile_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:User'])]
    private $profile;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: UserSite::class)]
    #[Groups(['get:User'])]
    private $userSites;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: ReportingFile::class)]
    #[Groups(['get:User'])]
    private $reportingFiles;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: AccommodationHistory::class)]
    #[Groups(['get:User'])]
    private $accommodationHistories;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: ElementHistory::class)]
    #[Groups(['get:User'])]
    private $elementHistories;

    #[ORM\Column(type: 'string', length: 180)]
    #[Groups(['get:User'])]
    private $username;

    #[ORM\Column(type: 'string', length: 50)]
    #[Groups(['get:User','get:ElementHistory'])]
    private $last_name;

    #[ORM\Column(type: 'string', length: 50)]
    #[Groups(['get:User','get:ElementHistory'])]
    private $first_name;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['get:User'])]
    private $is_active;

    #[ORM\Column(type: 'date', nullable: true)]
    #[Groups(['get:User'])]
    private $entry_date;

    #[ORM\Column(type: 'date', nullable: true)]
    #[Groups(['get:User'])]
    private $end_date;

    #[ORM\Column(type: 'string', length: 2,nullable: true)]
    #[Groups(['get:User'])]
    private $language;

    #[ORM\Column(type: 'string', length: 255,nullable: true)]
    #[Groups(['get:User'])]
    private $account_confirmation_token;

    #[ORM\Column(type: 'string', length: 255,nullable: true)]
    #[Groups(['get:User'])]
    private $account_reset_password_token;

    #[ORM\Column(type: 'boolean',nullable: true)]
    #[Groups(['get:User'])]
    private $confirmed_account;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:User'])]
    private $created_at;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:User'])]
    private $updated_at;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }
    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(?Profile $profile): self
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * @return Collection<int, UserSite>
     */
    public function getUserSites(): Collection
    {
        return $this->userSites;
    }

    public function addUserSite(UserSite $userSite): self
    {
        if (!$this->userSites->contains($userSite)) {
            $this->userSites[] = $userSite;
            $userSite->setUser($this);
        }

        return $this;
    }

    public function removeUserSite(UserSite $userSite): self
    {
        if ($this->userSites->removeElement($userSite)) {
            // set the owning side to null (unless already changed)
            if ($userSite->getUser() === $this) {
                $userSite->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ReportingFile>
     */
    public function getReportingFiles(): Collection
    {
        return $this->reportingFiles;
    }

    public function addReportingFile(ReportingFile $reportingFile): self
    {
        if (!$this->reportingFiles->contains($reportingFile)) {
            $this->reportingFiles[] = $reportingFile;
            $reportingFile->setUser($this);
        }

        return $this;
    }

    public function removeReportingFile(ReportingFile $reportingFile): self
    {
        if ($this->reportingFiles->removeElement($reportingFile)) {
            // set the owning side to null (unless already changed)
            if ($reportingFile->getUser() === $this) {
                $reportingFile->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, AccommodationHistory>
     */
    public function getAccommodationHistories(): Collection
    {
        return $this->accommodationHistories;
    }

    public function addAccommodationHistory(AccommodationHistory $accommodationHistory): self
    {
        if (!$this->accommodationHistories->contains($accommodationHistory)) {
            $this->accommodationHistories[] = $accommodationHistory;
            $accommodationHistory->setUser($this);
        }

        return $this;
    }

    public function removeAccommodationHistory(AccommodationHistory $accommodationHistory): self
    {
        if ($this->accommodationHistories->removeElement($accommodationHistory)) {
            // set the owning side to null (unless already changed)
            if ($accommodationHistory->getUser() === $this) {
                $accommodationHistory->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ElementHistory>
     */
    public function getElementHistories(): Collection
    {
        return $this->elementHistories;
    }

    public function addElementHistory(ElementHistory $elementHistory): self
    {
        if (!$this->elementHistories->contains($elementHistory)) {
            $this->elementHistories[] = $elementHistory;
            $elementHistory->setUser($this);
        }

        return $this;
    }

    public function removeElementHistory(ElementHistory $elementHistory): self
    {
        if ($this->elementHistories->removeElement($elementHistory)) {
            // set the owning side to null (unless already changed)
            if ($elementHistory->getUser() === $this) {
                $elementHistory->setUser(null);
            }
        }

        return $this;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getEntryDate(): ?\DateTimeInterface
    {
        return $this->entry_date;
    }

    public function setEntryDate(\DateTimeInterface $entry_date): self
    {
        $this->entry_date = $entry_date;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->end_date;
    }

    public function setEndDate(\DateTimeInterface $end_date): self
    {
        $this->end_date = $end_date;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getAccountConfirmationToken(): ?string
    {
        return $this->account_confirmation_token;
    }

    public function setAccountConfirmationToken(string $account_confirmation_token): self
    {
        $this->account_confirmation_token = $account_confirmation_token;

        return $this;
    }

    public function getAccountResetPasswordToken(): ?string
    {
        return $this->account_reset_password_token;
    }

    public function setAccountResetPasswordToken(string $account_reset_password_token): self
    {
        $this->account_reset_password_token = $account_reset_password_token;

        return $this;
    }

    public function getConfirmedAccount(): ?bool
    {
        return $this->confirmed_account;
    }

    public function setConfirmedAccount(bool $confirmed_account): self
    {
        $this->confirmed_account = $confirmed_account;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }
    public function getuser()
    {
        return $this;
    }
}

