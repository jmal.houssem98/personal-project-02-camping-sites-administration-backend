<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\CanvasProviderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CanvasProviderRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['get:CanvasProvider']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'name'=>'partial'])]
#[UniqueEntity('name')]


class CanvasProvider
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:CanvasProvider','get:Element'])]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    #[Groups(['get:CanvasProvider','get:Element'])]
    private $name;

    #[ORM\Column(type: 'string', length: 100)]
    #[Groups(['get:CanvasProvider'])]
    private $description;

    #[ORM\OneToMany(mappedBy: 'canvas_provider', targetEntity: Element::class)]
    #[Groups(['get:CanvasProvider'])]
    private $elements;

    public function __construct()
    {
        $this->elements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, Element>
     */
    public function getElements(): Collection
    {
        return $this->elements;
    }

    public function addElement(Element $element): self
    {
        if (!$this->elements->contains($element)) {
            $this->elements[] = $element;
            $element->setCanvasProvider($this);
        }

        return $this;
    }

    public function removeElement(Element $element): self
    {
        if ($this->elements->removeElement($element)) {
            // set the owning side to null (unless already changed)
            if ($element->getCanvasProvider() === $this) {
                $element->setCanvasProvider(null);
            }
        }

        return $this;
    }
}
