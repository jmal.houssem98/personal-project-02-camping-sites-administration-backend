<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\FeatureAccessRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: FeatureAccessRepository::class)]
#[ApiResource(
    paginationClientItemsPerPage: true,
    normalizationContext: ['groups' => ['get:FeatureAccess']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
)
]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'access_type' => "exact",'profile'=>"partial",'feature'=>"partial"])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at'])]


class FeatureAccess
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:FeatureAccess'])]
    private $id;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Groups(['get:FeatureAccess'])]
    private $access_type;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:FeatureAccess'])]
    private $created_at;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:FeatureAccess'])]
    private $updated_at;

    #[ORM\ManyToOne(targetEntity: Profile::class, inversedBy: 'featureAccesses')]
    #[ORM\JoinColumn(name: "profile_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:FeatureAccess'])]
    private $profile;

    #[ORM\ManyToOne(targetEntity: Feature::class, inversedBy: 'featureAccesses')]
    #[ORM\JoinColumn(name: "feature_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:FeatureAccess'])]
    private $feature;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccessType(): ?string
    {
        return $this->access_type;
    }

    public function setAccessType(?string $aceess_type): self
    {
        $this->access_type = $aceess_type;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(?Profile $profile): self
    {
        $this->profile = $profile;

        return $this;
    }

    public function getFeature(): ?Feature
    {
        return $this->feature;
    }

    public function setFeature(?Feature $feature): self
    {
        $this->feature = $feature;

        return $this;
    }
}
