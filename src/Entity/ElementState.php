<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ElementStateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ElementStateRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['get:ElementState']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
    collectionOperations: ['get', 'post'],
    itemOperations: ['get','put','patch']
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'name'=>'partial'])]
#[ApiFilter(BooleanFilter::class, properties: ['is_active','is_essential'])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at'])]
#[UniqueEntity('name')]
class ElementState
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:ElementState','get:Element','get:ElementHistory'])]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    #[Groups(['get:ElementState','get:Element','get:ElementHistory'])]
    private $name;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['get:ElementState'])]
    private $is_active;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['get:ElementState'])]
    private $is_essential;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:ElementState'])]
    private $created_at;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:ElementState'])]
    private $updated_at;

    #[ORM\OneToMany(mappedBy: 'state', targetEntity: Element::class)]
    #[Groups(['get:ElementState'])]
    private $elements;

    #[ORM\OneToMany(mappedBy: 'new_state', targetEntity: ElementHistory::class)]
    #[Groups(['get:ElementState'])]
    private $elementHistories;

    public function __construct()
    {
        $this->elements = new ArrayCollection();
        $this->elementHistories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getIsEssential(): ?bool
    {
        return $this->is_essential;
    }

    public function setIsEssential(bool $is_essential): self
    {
        $this->is_essential = $is_essential;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection<int, Element>
     */
    public function getElements(): Collection
    {
        return $this->elements;
    }

    public function addElement(Element $element): self
    {
        if (!$this->elements->contains($element)) {
            $this->elements[] = $element;
            $element->setState($this);
        }

        return $this;
    }

    public function removeElement(Element $element): self
    {
        if ($this->elements->removeElement($element)) {
            // set the owning side to null (unless already changed)
            if ($element->getState() === $this) {
                $element->setState(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ElementHistory>
     */
    public function getElementHistories(): Collection
    {
        return $this->elementHistories;
    }

    public function addElementHistory(ElementHistory $elementHistory): self
    {
        if (!$this->elementHistories->contains($elementHistory)) {
            $this->elementHistories[] = $elementHistory;
            $elementHistory->setNewState($this);
        }

        return $this;
    }

    public function removeElementHistory(ElementHistory $elementHistory): self
    {
        if ($this->elementHistories->removeElement($elementHistory)) {
            // set the owning side to null (unless already changed)
            if ($elementHistory->getNewState() === $this) {
                $elementHistory->setNewState(null);
            }
        }

        return $this;
    }
}
