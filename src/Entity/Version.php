<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\VersionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: VersionRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get',
          "getNotInitializedVersions"=>[
              "security"=>"is_granted('ROLE_USER')",
              "method"=>"GET",
              "path"=>"/versions/not_initialized",
              "normalization_context"=>[
                  "groups"=>["get:Version"],
                  "skip_null_values" => false
              ],
              "openapi_context"=>[
                     "parameters"=>[
                         [
                             "name"=>"siteId",
                             "in"=>"query",
                             "description"=>"Site ID",
                             "required"=>true,
                             "schema"=>["type"=> "integer", "example"=> "1"]
                         ]
                         ]
              ]
          ], 'post'],
    itemOperations: ['get','put','patch'],
    normalizationContext: ['groups' => ['get:Version'],],
    paginationClientItemsPerPage: true
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'name' => "partial",'range_id' => "exact", 'x3_reference' => "partial", 'removable' => "partial"])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at'])]
#[ApiFilter(BooleanFilter::class, properties: ['is_active'])]
#[ApiFilter(RangeFilter::class, properties: ['element_number'])]
#[UniqueEntity('name')]


class Version
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:Version','get:ElementType','get:Accommodation'])]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    #[Groups(['get:Version','get:ElementType','get:Accommodation'])]
    private $name;

    #[ORM\Column(type: 'integer')]
    #[Groups(['get:Version'])]
    private $element_number;

    #[ORM\Column(type: 'string', length: 50)]
    #[Groups(['get:Version'])]
    private $x3_reference;

    #[ORM\Column(type: 'string', length: 255,)]
    #[Groups(['get:Version'])]
    private $removable;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['get:Version','get:ElementType','get:Accommodation'])]
    private $is_active;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['get:Version'])]
    private $created_at;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['get:Version'])]
    private $updated_at;

    #[ORM\ManyToOne(targetEntity: Range::class, inversedBy: 'versions')]
    #[ORM\JoinColumn(name: "range_id_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Version'])]
    private $range_id;

    #[ORM\OneToMany(mappedBy: 'version', targetEntity: ElementType::class)]
    #[Groups(['get:Version'])]
    private $elementTypes;

    #[ORM\OneToMany(mappedBy: 'version', targetEntity: Accommodation::class)]
    #[Groups(['get:Version'])]
    private $accommodations;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
        $this->elementTypes = new ArrayCollection();
        $this->accommodations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getElementNumber(): ?int
    {
        return $this->element_number;
    }

    public function setElementNumber(int $element_number): self
    {
        $this->element_number = $element_number;

        return $this;
    }

    public function getX3Reference(): ?string
    {
        return $this->x3_reference;
    }

    public function setX3Reference(string $x3_reference): self
    {
        $this->x3_reference = $x3_reference;

        return $this;
    }

    public function getRemovable(): ?string
    {
        return $this->removable;
    }

    public function setRemovable(string $removable): self
    {
        $this->removable = $removable;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getRangeId(): ?Range
    {
        return $this->range_id;
    }

    public function setRangeId(?Range $range_id): self
    {
        $this->range_id = $range_id;

        return $this;
    }

    /**
     * @return Collection<int, ElementType>
     */
    public function getElementTypes(): Collection
    {
        return $this->elementTypes;
    }

    public function addElementType(ElementType $elementType): self
    {
        if (!$this->elementTypes->contains($elementType)) {
            $this->elementTypes[] = $elementType;
            $elementType->setVersion($this);
        }

        return $this;
    }

    public function removeElementType(ElementType $elementType): self
    {
        if ($this->elementTypes->removeElement($elementType)) {
            // set the owning side to null (unless already changed)
            if ($elementType->getVersion() === $this) {
                $elementType->setVersion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Accommodation>
     */
    public function getAccommodations(): Collection
    {
        return $this->accommodations;
    }

    public function addAccommodation(Accommodation $accommodation): self
    {
        if (!$this->accommodations->contains($accommodation)) {
            $this->accommodations[] = $accommodation;
            $accommodation->setVersion($this);
        }

        return $this;
    }

    public function removeAccommodation(Accommodation $accommodation): self
    {
        if ($this->accommodations->removeElement($accommodation)) {
            // set the owning side to null (unless already changed)
            if ($accommodation->getVersion() === $this) {
                $accommodation->setVersion(null);
            }
        }

        return $this;
    }
}
