<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\SiteGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: SiteGroupRepository::class)]
#[ApiResource(
    paginationClientItemsPerPage: true,
    normalizationContext: ['groups' => ['get:SiteGroup']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
)
]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'name' => "partial",'sites'=>"exact"])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at'])]
#[ApiFilter(BooleanFilter::class, properties: ['psd_flag'=>"exact"])]
#[UniqueEntity('name')]
class SiteGroup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:SiteGroup','get:Site'])]
    private $id;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Groups(['get:SiteGroup','get:Site'])]
    private $name;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['get:SiteGroup'])]
    private $psd_flag;

    #[ORM\OneToMany(mappedBy: 'site_group', targetEntity: Site::class)]
    #[Groups(['get:SiteGroup'])]
    private $sites;

    public function __construct()
    {
        $this->sites = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPsdFlag(): ?bool
    {
        return $this->psd_flag;
    }

    public function setPsdFlag(?bool $psd_flag): self
    {
        $this->psd_flag = $psd_flag;

        return $this;
    }

    /**
     * @return Collection<int, Site>
     */
    public function getSites(): Collection
    {
        return $this->sites;
    }

    public function addSite(Site $site): self
    {
        if (!$this->sites->contains($site)) {
            $this->sites[] = $site;
            $site->setSiteGroup($this);
        }

        return $this;
    }

    public function removeSite(Site $site): self
    {
        if ($this->sites->removeElement($site)) {
            // set the owning side to null (unless already changed)
            if ($site->getSiteGroup() === $this) {
                $site->setSiteGroup(null);
            }
        }

        return $this;
    }
}
