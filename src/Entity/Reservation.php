<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ReservationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ReservationRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['get:Reservation']],
    order: ["startDate" => "ASC"],
    paginationClientItemsPerPage: true,
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10

)]
#[ApiFilter(
    SearchFilter::class, properties: [
    'id' => "exact",
    'products' =>"partial",
    'status' =>"partial",
    'report' =>"partial",
    'client' =>"partial"

]
)]
#[ApiFilter(DateFilter::class, properties: ['startDate','endDate'])]

class Reservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:Reservation','get:Product'])]
    private $id;

    #[ORM\Column(type: 'date')]
    #[Groups(['get:Reservation','get:Product'])]
    private $startDate;

    #[ORM\Column(type: 'date')]
    #[Groups(['get:Reservation','get:Product'])]
    private $endDate;

    #[ORM\Column(type: 'string', length: 255,nullable: true)]
    #[Groups(['get:Reservation'])]
    private $startPeriod;

    #[ORM\Column(type: 'string', length: 255,nullable: true)]
    #[Groups(['get:Reservation'])]
    private $endPeriod;

    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'reservations')]
    #[Groups(['get:Reservation','get:Product'])]
    private $client;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['get:Reservation','get:Product'])]
    private $status;

    #[ORM\ManyToMany(targetEntity: Product::class, inversedBy: 'reservations')]
    #[Groups(['get:Reservation'])]
    private $products;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['get:Reservation'])]
    private $report;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getStartPeriod(): ?string
    {
        return $this->startPeriod;
    }

    public function setStartPeriod(string $startPeriod): self
    {
        $this->startPeriod = $startPeriod;

        return $this;
    }

    public function getEndPeriod(): ?string
    {
        return $this->endPeriod;
    }

    public function setEndPeriod(string $endPeriod): self
    {
        $this->endPeriod = $endPeriod;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        $this->products->removeElement($product);

        return $this;
    }

    public function getReport(): ?string
    {
        return $this->report;
    }

    public function setReport(?string $report): self
    {
        $this->report = $report;

        return $this;
    }
}
