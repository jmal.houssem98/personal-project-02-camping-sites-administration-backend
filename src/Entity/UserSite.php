<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\UserSiteRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: UserSiteRepository::class)]
#[ApiResource(
    paginationClientItemsPerPage: true,
    normalizationContext: ['groups' => ['get:UserSite']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
)
]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'user' => "exact",'site'=>"exact"])]


class UserSite
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:UserSite'])]
    private $id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'userSites')]
    #[ORM\JoinColumn(name: "user_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:UserSite'])]
    private $user;

    #[ORM\ManyToOne(targetEntity: Site::class, inversedBy: 'userSites')]
    #[ORM\JoinColumn(name: "site_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:UserSite'])]
    private $site;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getSite(): ?Site
    {
        return $this->site;
    }

    public function setSite(?Site $site): self
    {
        $this->site = $site;

        return $this;
    }
}
