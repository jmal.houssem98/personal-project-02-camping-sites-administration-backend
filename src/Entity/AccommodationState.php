<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\AccommodationStateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: AccommodationStateRepository::class)]
#[ApiResource(
    paginationClientItemsPerPage: true,
    normalizationContext: ['groups' => ['get:AccommodationState']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
    collectionOperations: ['get', 'post'],
    itemOperations: ['get','put','patch']
)
]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'name' => "partial"])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at'])]
#[ApiFilter(BooleanFilter::class, properties: ['is_active'=>"exact"])]
#[UniqueEntity('name')]



class AccommodationState
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:AccommodationState','get:Accommodation'])]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    #[Groups(['get:AccommodationState','get:Accommodation'])]
    private $name;

    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['get:AccommodationState'])]
    private $is_active;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:AccommodationState'])]
    private $created_at;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:AccommodationState'])]
    private $updated_at;

    #[ORM\OneToMany(mappedBy: 'state', targetEntity: Accommodation::class)]
    #[ORM\JoinColumn(name: "accommodations_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:AccommodationState'])]
    private $accommodations;

    #[ORM\OneToMany(mappedBy: 'new_state', targetEntity: AccommodationHistory::class)]
    private $accommodationHistories;

    public function __construct()
    {
        $this->accommodations = new ArrayCollection();
        $this->accommodationHistories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection<int, Accommodation>
     */
    public function getAccommodations(): Collection
    {
        return $this->accommodations;
    }

    public function addAccommodation(Accommodation $accommodation): self
    {
        if (!$this->accommodations->contains($accommodation)) {
            $this->accommodations[] = $accommodation;
            $accommodation->setState($this);
        }

        return $this;
    }

    public function removeAccommodation(Accommodation $accommodation): self
    {
        if ($this->accommodations->removeElement($accommodation)) {
            // set the owning side to null (unless already changed)
            if ($accommodation->getState() === $this) {
                $accommodation->setState(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, AccommodationHistory>
     */
    public function getAccommodationHistories(): Collection
    {
        return $this->accommodationHistories;
    }

    public function addAccommodationHistory(AccommodationHistory $accommodationHistory): self
    {
        if (!$this->accommodationHistories->contains($accommodationHistory)) {
            $this->accommodationHistories[] = $accommodationHistory;
            $accommodationHistory->setNewState($this);
        }

        return $this;
    }

    public function removeAccommodationHistory(AccommodationHistory $accommodationHistory): self
    {
        if ($this->accommodationHistories->removeElement($accommodationHistory)) {
            // set the owning side to null (unless already changed)
            if ($accommodationHistory->getNewState() === $this) {
                $accommodationHistory->setNewState(null);
            }
        }

        return $this;
    }
}
