<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ElementHistoryRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ElementHistoryRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['get:ElementHistory']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'element' => "exact",'new_state' => "exact"])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at'])]
class ElementHistory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:ElementHistory'])]
    private $id;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['get:ElementHistory'])]
    private $created_at;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['get:ElementHistory'])]
    private $updated_at;

    #[ORM\ManyToOne(targetEntity: Element::class, inversedBy: 'elementHistories')]
    #[ORM\JoinColumn(name: "element_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:ElementHistory'])]
    private $element;

    #[ORM\ManyToOne(targetEntity: ElementState::class, inversedBy: 'elementHistories')]
    #[ORM\JoinColumn(name: "new_state_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:ElementHistory'])]
    private $new_state;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'elementHistories')]
    #[ORM\JoinColumn(name: "user_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:ElementHistory'])]
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getElement(): ?Element
    {
        return $this->element;
    }

    public function setElement(?Element $element): self
    {
        $this->element = $element;

        return $this;
    }

    public function getNewState(): ?ElementState
    {
        return $this->new_state;
    }

    public function setNewState(?ElementState $new_state): self
    {
        $this->new_state = $new_state;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
