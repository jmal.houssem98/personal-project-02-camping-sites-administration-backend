<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ReparationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ReparationRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['get:Reparation']],
    paginationItemsPerPage: 5,
    paginationMaximumItemsPerPage: 10,
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'service_provider' => "partial",'comment' => "partial"])]
#[ApiFilter(BooleanFilter::class, properties: ['sinister'])]
#[ApiFilter(DateFilter::class, properties: ['reparation_sending_date','reparation_end_date'])]
#[ApiFilter(RangeFilter::class, properties: ['amount'])]

class Reparation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:Reparation','get:Element'])]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    #[Groups(['get:Reparation'])]
    private $service_provider;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['get:Reparation'])]
    private $reparation_sending_date;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['get:Reparation'])]
    private $reparation_end_date;

    #[ORM\Column(type: 'string', length: 150)]
    #[Groups(['get:Reparation'])]
    private $comment;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['get:Reparation'])]
    private $sinister;

    #[ORM\Column(type: 'integer')]
    #[Groups(['get:Reparation'])]
    private $amount;

    #[ORM\ManyToOne(targetEntity: Element::class, inversedBy: 'reparations')]
    #[ORM\JoinColumn(name: "element_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Reparation'])]
    private $element;

    #[ORM\ManyToOne(targetEntity: ReparationType::class, inversedBy: 'reparations')]
    #[ORM\JoinColumn(name: "reparation_type_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Reparation'])]
    private $reparation_type;

    #[ORM\OneToMany(mappedBy: 'reparation', targetEntity: ReparationFile::class)]
    #[Groups(['get:Reparation'])]
    private $reparationFiles;

    public function __construct()
    {
        $this->reparationFiles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getServiceProvider(): ?string
    {
        return $this->service_provider;
    }

    public function setServiceProvider(string $service_provider): self
    {
        $this->service_provider = $service_provider;

        return $this;
    }

    public function getReparationSendingDate(): ?\DateTimeInterface
    {
        return $this->reparation_sending_date;
    }

    public function setReparationSendingDate(\DateTimeInterface $reparation_sending_date): self
    {
        $this->reparation_sending_date = $reparation_sending_date;

        return $this;
    }

    public function getReparationEndDate(): ?\DateTimeInterface
    {
        return $this->reparation_end_date;
    }

    public function setReparationEndDate(\DateTimeInterface $reparation_end_date): self
    {
        $this->reparation_end_date = $reparation_end_date;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getSinister(): ?bool
    {
        return $this->sinister;
    }

    public function setSinister(bool $sinister): self
    {
        $this->sinister = $sinister;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getElement(): ?Element
    {
        return $this->element;
    }

    public function setElement(?Element $element): self
    {
        $this->element = $element;

        return $this;
    }

    public function getReparationType(): ?ReparationType
    {
        return $this->reparation_type;
    }

    public function setReparationType(?ReparationType $reparation_type): self
    {
        $this->reparation_type = $reparation_type;

        return $this;
    }

    /**
     * @return Collection<int, ReparationFile>
     */
    public function getReparationFiles(): Collection
    {
        return $this->reparationFiles;
    }

    public function addReparationFile(ReparationFile $reparationFile): self
    {
        if (!$this->reparationFiles->contains($reparationFile)) {
            $this->reparationFiles[] = $reparationFile;
            $reparationFile->setReparation($this);
        }

        return $this;
    }

    public function removeReparationFile(ReparationFile $reparationFile): self
    {
        if ($this->reparationFiles->removeElement($reparationFile)) {
            // set the owning side to null (unless already changed)
            if ($reparationFile->getReparation() === $this) {
                $reparationFile->setReparation(null);
            }
        }

        return $this;
    }
}
