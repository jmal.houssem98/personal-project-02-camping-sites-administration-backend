<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\LifeSheetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: LifeSheetRepository::class)]
#[ApiResource(
    paginationClientItemsPerPage: true,
    normalizationContext: ['groups' => ['get:LifeSheet']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
)
]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'file_path' => "partial",'accommodationHistories'=>"partial"])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at'])]
class LifeSheet
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:LifeSheet'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['get:LifeSheet'])]
    private $file_path;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:LifeSheet'])]
    private $created_at;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:LifeSheet'])]
    private $updated_at;

    #[ORM\OneToMany(mappedBy: 'life_sheet', targetEntity: AccommodationHistory::class)]
    #[Groups(['get:LifeSheet'])]
    private $accommodationHistories;

    public function __construct()
    {
        $this->accommodationHistories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFilePath(): ?string
    {
        return $this->file_path;
    }

    public function setFilePath(?string $file_path): self
    {
        $this->file_path = $file_path;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection<int, AccommodationHistory>
     */
    public function getAccommodationHistories(): Collection
    {
        return $this->accommodationHistories;
    }

    public function addAccommodationHistory(AccommodationHistory $accommodationHistory): self
    {
        if (!$this->accommodationHistories->contains($accommodationHistory)) {
            $this->accommodationHistories[] = $accommodationHistory;
            $accommodationHistory->setLifeSheet($this);
        }

        return $this;
    }

    public function removeAccommodationHistory(AccommodationHistory $accommodationHistory): self
    {
        if ($this->accommodationHistories->removeElement($accommodationHistory)) {
            // set the owning side to null (unless already changed)
            if ($accommodationHistory->getLifeSheet() === $this) {
                $accommodationHistory->setLifeSheet(null);
            }
        }

        return $this;
    }
}
