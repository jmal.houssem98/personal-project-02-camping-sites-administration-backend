<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\RentalTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RentalTypeRepository::class)]
#[ApiResource(
    collectionOperations: ['get', 'post'],
    itemOperations: ['get','put','patch'] ,
    normalizationContext: ['groups' => ['get:rentalType']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,

)]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact", 'name' => "partial"])]
#[ApiFilter(BooleanFilter::class, properties: ['is_active'])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at'])]
#[UniqueEntity('name')]
class RentalType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:rentalType','get:reparationType','get:AccommodationType'])]
    private $id;

   #[Groups(["write:rentalType:collecton",'get:rentalType','get:reparationType','get:AccommodationType'])]
    #[ORM\Column(type: 'string', length: 50)]
    private $name;

    #[Groups(["write:rentalType:collecton",'get:rentalType','get:reparationType','get:AccommodationType'])]
    #[ORM\Column(type: 'boolean')]
    private $is_active;

    #[Groups(['get:rentalType'])]
    #[ORM\Column(type: 'datetime')]
    private $created_at;

    #[Groups(["write:rentalType:collecton",'get:rentalType'])]
    #[ORM\Column(type: 'datetime')]
    private $updated_at;

    #[ORM\OneToMany(mappedBy: 'rental_type', targetEntity: ReparationType::class)]
    #[Groups(['get:rentalType'])]
    private $reparationTypes;

    #[ORM\OneToMany(mappedBy: 'rental_type', targetEntity: AccommodationType::class)]
    #[Groups(['get:rentalType'])]
    private $accomodationTypes;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
        $this->reparationTypes = new ArrayCollection();
        $this->accomodationTypes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection<int, ReparationType>
     */
    public function getReparationTypes(): Collection
    {
        return $this->reparationTypes;
    }

    public function addReparationType(ReparationType $reparationType): self
    {
        if (!$this->reparationTypes->contains($reparationType)) {
            $this->reparationTypes[] = $reparationType;
            $reparationType->setRentalType($this);
        }

        return $this;
    }

    public function removeReparationType(ReparationType $reparationType): self
    {
        if ($this->reparationTypes->removeElement($reparationType)) {
            // set the owning side to null (unless already changed)
            if ($reparationType->getRentalType() === $this) {
                $reparationType->setRentalType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, AccommodationType>
     */
    public function getAccomodationTypes(): Collection
    {
        return $this->accomodationTypes;
    }

    public function addAccomodationType(AccommodationType $accomodationType): self
    {
        if (!$this->accomodationTypes->contains($accomodationType)) {
            $this->accomodationTypes[] = $accomodationType;
            $accomodationType->setRentalType($this);
        }

        return $this;
    }

    public function removeAccomodationType(AccommodationType $accomodationType): self
    {
        if ($this->accomodationTypes->removeElement($accomodationType)) {
            // set the owning side to null (unless already changed)
            if ($accomodationType->getRentalType() === $this) {
                $accomodationType->setRentalType(null);
            }
        }

        return $this;
    }

}
