<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\PlaceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PlaceRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get',
        "getNotInitializedPlaces"=>[
            "security"=>"is_granted('ROLE_USER')",
            "method"=>"GET",
           "path"=>"/places/not_initialized",
              "normalization_context"=>[
                  "groups"=>["place:collection:get"],
                  "skip_null_values" => false
                 ],
              "openapi_context"=>[
                "parameters"=>[
                    [
                        "name"=>"siteId",
                        "in"=>"query",
                        "description"=>"Site ID",
                        "required"=>true,
                        "schema"=>["type"=> "integer", "example"=> "1"]
                      ],
                    [
                        "name"=>"versionId",
                        "in"=>"query",
                        "description"=>"Version ID",
                        "required"=>false,
                        "schema"=>["type"=> "integer", "example"=> "1"]
                      ],
                ],
              ],
          ]
        ,'post'],
    itemOperations: ['get','put','patch'],
    normalizationContext: ['groups' => ['get:Place']],
    paginationClientItemsPerPage: true,
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10
)
]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'name' => "partial",'place_type'=>"partial",'site'=>"exact"])]
#[ApiFilter(RangeFilter::class, properties: ['number'])]
#[ApiFilter(BooleanFilter::class, properties: ['is_active'=>"exact"])]
#[UniqueEntity('name')]

class Place
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:Place','get:Accommodation','place:collection:get'])]
    private $id;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Groups(['get:Place','get:Accommodation','place:collection:get'])]
    private $name;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Groups(['get:Place'])]
    private $number;

    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['get:Place'])]
    private $is_active;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:Place'])]
    private $created_at;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:Place'])]
    private $updated_at;

    #[ORM\ManyToOne(targetEntity: PlaceType::class, inversedBy: 'places')]
    #[ORM\JoinColumn(name: "place_type_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Place'])]
    private $place_type;

    #[ORM\ManyToOne(targetEntity: Site::class, inversedBy: 'places')]
    #[ORM\JoinColumn(name: "site_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Place','get:Accommodation'])]
    private $site;

    #[ORM\OneToMany(mappedBy: 'place', targetEntity: Accommodation::class)]
    #[Groups(['get:Place'])]
    private $accommodations;

    public function __construct()
    {
        $this->accommodations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(?string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getPlaceType(): ?PlaceType
    {
        return $this->place_type;
    }

    public function setPlaceType(?PlaceType $place_type): self
    {
        $this->place_type = $place_type;

        return $this;
    }

    public function getSite(): ?Site
    {
        return $this->site;
    }

    public function setSite(?Site $site): self
    {
        $this->site = $site;

        return $this;
    }

    /**
     * @return Collection<int, Accommodation>
     */
    public function getAccommodations(): Collection
    {
        return $this->accommodations;
    }

    public function addAccommodation(Accommodation $accommodation): self
    {
        if (!$this->accommodations->contains($accommodation)) {
            $this->accommodations[] = $accommodation;
            $accommodation->setPlace($this);
        }

        return $this;
    }

    public function removeAccommodation(Accommodation $accommodation): self
    {
        if ($this->accommodations->removeElement($accommodation)) {
            // set the owning side to null (unless already changed)
            if ($accommodation->getPlace() === $this) {
                $accommodation->setPlace(null);
            }
        }

        return $this;
    }
}
