<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ReparationTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ReparationTypeRepository::class)]
#[ApiResource(
    collectionOperations: ['get', 'post'],
    itemOperations: ['get','put','patch'],
    normalizationContext: ['groups' => ['get:reparationType']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,

)]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact", 'name' => "partial", 'rental_type' => "exact"])]
#[ApiFilter(BooleanFilter::class, properties: ['is_active'=>"exact"])]
#[UniqueEntity('name')]
class ReparationType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[Groups(['get:reparationType','get:Reparation'])]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups(['get:reparationType','get:Reparation'])]
    #[ORM\Column(type: 'string', length: 100)]
    private $name;

    #[Groups(['get:reparationType'])]
    #[ORM\Column(type: 'boolean')]
    private $is_active;

    #[Groups(['get:reparationType'])]
    #[ORM\ManyToOne(targetEntity: RentalType::class, inversedBy: 'reparationTypes')]
    #[ORM\JoinColumn(name: "rental_type_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    private $rental_type;

    #[ORM\OneToMany(mappedBy: 'reparation_type', targetEntity: Reparation::class)]
    #[Groups(['get:reparationType'])]
    private $reparations;

    public function __construct()
    {
        $this->reparations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getRentalType(): ?RentalType
    {
        return $this->rental_type;
    }

    public function setRentalType(?RentalType $rental_type): self
    {
        $this->rental_type = $rental_type;

        return $this;
    }

    /**
     * @return Collection<int, Reparation>
     */
    public function getReparations(): Collection
    {
        return $this->reparations;
    }

    public function addReparation(Reparation $reparation): self
    {
        if (!$this->reparations->contains($reparation)) {
            $this->reparations[] = $reparation;
            $reparation->setReparationType($this);
        }

        return $this;
    }

    public function removeReparation(Reparation $reparation): self
    {
        if ($this->reparations->removeElement($reparation)) {
            // set the owning side to null (unless already changed)
            if ($reparation->getReparationType() === $this) {
                $reparation->setReparationType(null);
            }
        }

        return $this;
    }
}
