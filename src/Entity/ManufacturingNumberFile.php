<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ManufacturingNumberFileRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ManufacturingNumberFileRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['get:ManufacturingNumberFile']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'file_path' => "partial",'element' => "exact"])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at'])]
class ManufacturingNumberFile
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:ManufacturingNumberFile'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['get:ManufacturingNumberFile'])]
    private $file_path;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['get:ManufacturingNumberFile'])]
    private $created_at;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['get:ManufacturingNumberFile'])]
    private $updated_at;

    #[ORM\ManyToOne(targetEntity: Element::class, inversedBy: 'manufacturingNumberFiles')]
    #[ORM\JoinColumn(name: "element_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:ManufacturingNumberFile'])]
    private $element;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFilePath(): ?string
    {
        return $this->file_path;
    }

    public function setFilePath(string $file_path): self
    {
        $this->file_path = $file_path;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getElement(): ?Element
    {
        return $this->element;
    }

    public function setElement(?Element $element): self
    {
        $this->element = $element;

        return $this;
    }
}
