<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ProfileRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ProfileRepository::class)]
#[ApiResource(
    paginationClientItemsPerPage: true,
    normalizationContext: ['groups' => ['get:Profile']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
    collectionOperations: ['get', 'post'],
    itemOperations: ['get','put','patch']
)
]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'name' => "partial",'description'=>"partial",'users'=>"partial",'featureAccesses'=>"partial"])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at'])]
#[ApiFilter(BooleanFilter::class, properties: ['is_active'=>"exact"])]
#[UniqueEntity('name')]
class Profile
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:Profile'])]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    #[Groups(['get:Profile'])]
    private $name;

    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['get:Profile'])]
    private $is_active;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Groups(['get:Profile'])]
    private $description;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:Profile'])]
    private $created_at;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:Profile'])]
    private $updated_at;

    #[ORM\OneToMany(mappedBy: 'profile', targetEntity: User::class)]
    #[Groups(['get:Profile'])]
    private $users;

    #[ORM\OneToMany(mappedBy: 'profile', targetEntity: FeatureAccess::class)]
    #[Groups(['get:Profile'])]
    private $featureAccesses;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->featureAccesses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setProfile($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getProfile() === $this) {
                $user->setProfile(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, FeatureAccess>
     */
    public function getFeatureAccesses(): Collection
    {
        return $this->featureAccesses;
    }

    public function addFeatureAccess(FeatureAccess $featureAccess): self
    {
        if (!$this->featureAccesses->contains($featureAccess)) {
            $this->featureAccesses[] = $featureAccess;
            $featureAccess->setProfile($this);
        }

        return $this;
    }

    public function removeFeatureAccess(FeatureAccess $featureAccess): self
    {
        if ($this->featureAccesses->removeElement($featureAccess)) {
            // set the owning side to null (unless already changed)
            if ($featureAccess->getProfile() === $this) {
                $featureAccess->setProfile(null);
            }
        }

        return $this;
    }
}
