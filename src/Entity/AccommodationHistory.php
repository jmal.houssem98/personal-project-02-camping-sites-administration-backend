<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\AccommodationHistoryRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: AccommodationHistoryRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['get:AccommodationHistory']],
    paginationClientItemsPerPage: true,
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
)
]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'user' => "partial",'life_sheet' => "exact"])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at'])]
#[ApiFilter(BooleanFilter::class, properties: ['is_life_sheet_ok'=>"exact"])]


class AccommodationHistory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:AccommodationHistory'])]
    private $id;

    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['get:AccommodationHistory'])]
    private $is_life_sheet_ok;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:AccommodationHistory'])]
    private $created_at;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:AccommodationHistory'])]
    private $updated_at;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'accommodationHistories')]
    #[ORM\JoinColumn(name: "user_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:AccommodationHistory'])]
    private $user;

    #[ORM\ManyToOne(targetEntity: LifeSheet::class, inversedBy: 'accommodationHistories')]
    #[ORM\JoinColumn(name: "life_sheet_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:AccommodationHistory'])]
    private $life_sheet;

    #[ORM\ManyToOne(targetEntity: Accommodation::class, inversedBy: 'accommodationHistories')]
    #[ORM\JoinColumn(name: "accommodation_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:AccommodationHistory'])]
    private $accommodation;

    #[ORM\ManyToOne(targetEntity: AccommodationState::class, inversedBy: 'accommodationHistories')]
    #[ORM\JoinColumn(name: "accommodation_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:AccommodationHistory'])]
    private $new_state;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsLifeSheetOk(): ?bool
    {
        return $this->is_life_sheet_ok;
    }

    public function setIsLifeSheetOk(?bool $is_life_sheet_ok): self
    {
        $this->is_life_sheet_ok = $is_life_sheet_ok;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getLifeSheet(): ?LifeSheet
    {
        return $this->life_sheet;
    }

    public function setLifeSheet(?LifeSheet $life_sheet): self
    {
        $this->life_sheet = $life_sheet;

        return $this;
    }

    public function getAccommodation(): ?Accommodation
    {
        return $this->accommodation;
    }

    public function setAccommodation(?Accommodation $accommodation): self
    {
        $this->accommodation = $accommodation;

        return $this;
    }

    public function getNewState(): ?AccommodationState
    {
        return $this->new_state;
    }

    public function setNewState(?AccommodationState $new_state): self
    {
        $this->new_state = $new_state;

        return $this;
    }
}
