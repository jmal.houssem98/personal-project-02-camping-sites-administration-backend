<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\InstallationTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: InstallationTypeRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['get:InstallationType']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
    collectionOperations: ['get', 'post'],
    itemOperations: ['get','put','patch']
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'description' => "partial",'name'=>'partial'])]
#[ApiFilter(BooleanFilter::class, properties: ['is_active'])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at'])]
#[UniqueEntity('name')]

class InstallationType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:InstallationType','get:Accommodation'])]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    #[Groups(['get:InstallationType','get:Accommodation'])]
    private $name;

    #[ORM\Column(type: 'string', length: 50)]
    #[Groups(['get:InstallationType',])]
    private $description;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['get:InstallationType','get:Accommodation'])]
    private $is_active;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['get:InstallationType'])]
    private $created_at;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['get:InstallationType'])]
    private $updated_at;

    #[ORM\OneToMany(mappedBy: 'installation_type', targetEntity: Accommodation::class)]
    #[Groups(['get:InstallationType'])]
    private $accommodations;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
        $this->accommodations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection<int, Accommodation>
     */
    public function getAccommodations(): Collection
    {
        return $this->accommodations;
    }

    public function addAccommodation(Accommodation $accommodation): self
    {
        if (!$this->accommodations->contains($accommodation)) {
            $this->accommodations[] = $accommodation;
            $accommodation->setInstallationType($this);
        }

        return $this;
    }

    public function removeAccommodation(Accommodation $accommodation): self
    {
        if ($this->accommodations->removeElement($accommodation)) {
            // set the owning side to null (unless already changed)
            if ($accommodation->getInstallationType() === $this) {
                $accommodation->setInstallationType(null);
            }
        }

        return $this;
    }
}
