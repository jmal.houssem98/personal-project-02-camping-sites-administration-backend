<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\InvoiceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: InvoiceRepository::class)]
#[ApiResource(
    denormalizationContext: ['groups' => ['post:Invoice']],
    normalizationContext: ['groups' => ['get:Invoice']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'accommodation_type' => "exact"])]
#[ApiFilter(BooleanFilter::class, properties: ['is_active'=>"exact"])]
#[ApiFilter(RangeFilter::class, properties: ['age','percentage'])]
class Invoice
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:Invoice'])]
    private $id;

    #[ORM\Column(type: 'integer')]
    #[Groups(['get:Invoice','post:Invoice'])]
    private $age;

    #[ORM\Column(type: 'integer')]
    #[Groups(['get:Invoice','post:Invoice'])]
    private $percentage;

    #[ORM\ManyToOne(targetEntity: AccommodationType::class, inversedBy: 'invoices')]
    #[ORM\JoinColumn(name: "accommodation_type_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Invoice','post:Invoice'])]
    private $accommodation_type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getPercentage(): ?int
    {
        return $this->percentage;
    }

    public function setPercentage(int $percentage): self
    {
        $this->percentage = $percentage;

        return $this;
    }

    public function getAccommodationType(): ?AccommodationType
    {
        return $this->accommodation_type;
    }

    public function setAccommodationType(?AccommodationType $accommodation_type): self
    {
        $this->accommodation_type = $accommodation_type;

        return $this;
    }
}
