<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\PricingRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PricingRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['get:Pricing']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'element_type' => "exact"])]
#[ApiFilter(RangeFilter::class, properties: ['year','cost'])]
class Pricing
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups('get:Pricing')]
    private $id;

    #[ORM\Column(type: 'integer')]
    #[Groups('get:Pricing')]
    private $year;

    #[ORM\Column(type: 'integer')]
    #[Groups('get:Pricing')]
    private $cost;

    #[ORM\ManyToOne(targetEntity: ElementType::class, inversedBy: 'pricings')]
    #[ORM\JoinColumn(name: "element_type_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups('get:Pricing')]
    private $element_type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getCost(): ?int
    {
        return $this->cost;
    }

    public function setCost(int $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getElementType(): ?ElementType
    {
        return $this->element_type;
    }

    public function setElementType(?ElementType $element_type): self
    {
        $this->element_type = $element_type;

        return $this;
    }
}
