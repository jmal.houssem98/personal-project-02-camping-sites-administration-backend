<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ElementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ElementRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['get:Element']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'recognition_code' => "exact",'element_type' => "exact",'canvas_provider' => "exact",
    'manufacturer' => "exact",'state' => "exact",'accommodation' => "exact"])]
#[ApiFilter(BooleanFilter::class, properties: ['sinister','is_guaranteed'])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at','installation_date','scrapping_date','guarantee_end_date'])]

class Element
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:Element','get:Reparation','get:Loan','get:Accommodation'])]
    private $id;

    #[ORM\Column(type: 'date')]
    #[Groups(['get:Element'])]
    private $installation_date;

    #[ORM\Column(type: 'date')]
    #[Groups(['get:Element'])]
    private $scrapping_date;

    #[ORM\Column(type: 'string', length: 50,  nullable: true)]
    #[Groups(['get:Element','get:Accommodation'])]
    private $recognition_code;

    #[ORM\Column(type: 'date')]
    #[Groups(['get:Element'])]
    private $guarantee_end_date;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['get:Element'])]
    private $sinister;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['get:Element'])]
    private $is_guaranteed;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:Element'])]
    private $created_at;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:Element'])]
    private $updated_at;

    #[ORM\ManyToOne(targetEntity: ElementType::class, inversedBy: 'elements')]
    #[ORM\JoinColumn(name: "element_type_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Element','get:Accommodation'])]
    private $element_type;

    #[ORM\ManyToOne(targetEntity: CanvasProvider::class, inversedBy: 'elements')]
    #[ORM\JoinColumn(name: "canvas_provider_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Element'])]
    private $canvas_provider;

    #[ORM\ManyToOne(targetEntity: Manufacturer::class, inversedBy: 'elements')]
    #[ORM\JoinColumn(name: "manufacturer_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Element'])]
    private $manufacturer;

    #[ORM\ManyToOne(targetEntity: ElementState::class, inversedBy: 'elements')]
    #[ORM\JoinColumn(name: "state_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Element'])]
    private $state;

    #[ORM\ManyToOne(targetEntity: Accommodation::class, inversedBy: 'elements')]
    #[ORM\JoinColumn(name: "accommodation_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Element'])]
    private $accommodation;

    #[ORM\OneToMany(mappedBy: 'element', targetEntity: Reparation::class)]
    #[Groups(['get:Element'])]
    private $reparations;

    #[ORM\OneToMany(mappedBy: 'element', targetEntity: ManufacturingNumberFile::class)]
    #[Groups(['get:Element'])]
    private $manufacturingNumberFiles;

    #[ORM\OneToMany(mappedBy: 'element', targetEntity: ElementHistory::class)]
    #[Groups(['get:Element'])]
    private $elementHistories;

    #[ORM\OneToMany(mappedBy: 'element', targetEntity: Loan::class)]
    #[Groups(['get:Element'])]
    private $loans;

    public function __construct()
    {
        $this->reparations = new ArrayCollection();
        $this->manufacturingNumberFiles = new ArrayCollection();
        $this->elementHistories = new ArrayCollection();
        $this->loans = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInstallationDate(): ?\DateTimeInterface
    {
        return $this->installation_date;
    }

    public function setInstallationDate(\DateTimeInterface $installation_date): self
    {
        $this->installation_date = $installation_date;

        return $this;
    }

    public function getScrappingDate(): ?\DateTimeInterface
    {
        return $this->scrapping_date;
    }

    public function setScrappingDate(\DateTimeInterface $scrapping_date): self
    {
        $this->scrapping_date = $scrapping_date;

        return $this;
    }

    public function getRecognitionCode(): ?string
    {
        return $this->recognition_code;
    }

    public function setRecognitionCode(string $recognition_code): self
    {
        $this->recognition_code = $recognition_code;

        return $this;
    }

    public function getGuaranteeEndDate(): ?\DateTimeInterface
    {
        return $this->guarantee_end_date;
    }

    public function setGuaranteeEndDate(\DateTimeInterface $guarantee_end_date): self
    {
        $this->guarantee_end_date = $guarantee_end_date;

        return $this;
    }

    public function getSinister(): ?bool
    {
        return $this->sinister;
    }

    public function setSinister(bool $sinister): self
    {
        $this->sinister = $sinister;

        return $this;
    }

    public function getIsGuaranteed(): ?bool
    {
        return $this->is_guaranteed;
    }

    public function setIsGuaranteed(bool $is_guaranteed): self
    {
        $this->is_guaranteed = $is_guaranteed;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getElementType(): ?ElementType
    {
        return $this->element_type;
    }

    public function setElementType(?ElementType $element_type): self
    {
        $this->element_type = $element_type;

        return $this;
    }

    public function getCanvasProvider(): ?CanvasProvider
    {
        return $this->canvas_provider;
    }

    public function setCanvasProvider(?CanvasProvider $canvas_provider): self
    {
        $this->canvas_provider = $canvas_provider;

        return $this;
    }

    public function getManufacturer(): ?Manufacturer
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?Manufacturer $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    public function getState(): ?ElementState
    {
        return $this->state;
    }

    public function setState(?ElementState $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getAccommodation(): ?Accommodation
    {
        return $this->accommodation;
    }

    public function setAccommodation(?Accommodation $accommodation): self
    {
        $this->accommodation = $accommodation;

        return $this;
    }

    /**
     * @return Collection<int, Reparation>
     */
    public function getReparations(): Collection
    {
        return $this->reparations;
    }

    public function addReparation(Reparation $reparation): self
    {
        if (!$this->reparations->contains($reparation)) {
            $this->reparations[] = $reparation;
            $reparation->setElement($this);
        }

        return $this;
    }

    public function removeReparation(Reparation $reparation): self
    {
        if ($this->reparations->removeElement($reparation)) {
            // set the owning side to null (unless already changed)
            if ($reparation->getElement() === $this) {
                $reparation->setElement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ManufacturingNumberFile>
     */
    public function getManufacturingNumberFiles(): Collection
    {
        return $this->manufacturingNumberFiles;
    }

    public function addManufacturingNumberFile(ManufacturingNumberFile $manufacturingNumberFile): self
    {
        if (!$this->manufacturingNumberFiles->contains($manufacturingNumberFile)) {
            $this->manufacturingNumberFiles[] = $manufacturingNumberFile;
            $manufacturingNumberFile->setElement($this);
        }

        return $this;
    }

    public function removeManufacturingNumberFile(ManufacturingNumberFile $manufacturingNumberFile): self
    {
        if ($this->manufacturingNumberFiles->removeElement($manufacturingNumberFile)) {
            // set the owning side to null (unless already changed)
            if ($manufacturingNumberFile->getElement() === $this) {
                $manufacturingNumberFile->setElement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ElementHistory>
     */
    public function getElementHistories(): Collection
    {
        return $this->elementHistories;
    }

    public function addElementHistory(ElementHistory $elementHistory): self
    {
        if (!$this->elementHistories->contains($elementHistory)) {
            $this->elementHistories[] = $elementHistory;
            $elementHistory->setElement($this);
        }

        return $this;
    }

    public function removeElementHistory(ElementHistory $elementHistory): self
    {
        if ($this->elementHistories->removeElement($elementHistory)) {
            // set the owning side to null (unless already changed)
            if ($elementHistory->getElement() === $this) {
                $elementHistory->setElement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Loan>
     */
    public function getLoans(): Collection
    {
        return $this->loans;
    }

    public function addLoan(Loan $loan): self
    {
        if (!$this->loans->contains($loan)) {
            $this->loans[] = $loan;
            $loan->setElement($this);
        }

        return $this;
    }

    public function removeLoan(Loan $loan): self
    {
        if ($this->loans->removeElement($loan)) {
            // set the owning side to null (unless already changed)
            if ($loan->getElement() === $this) {
                $loan->setElement(null);
            }
        }

        return $this;
    }
}
