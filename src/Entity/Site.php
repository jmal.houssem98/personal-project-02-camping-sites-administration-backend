<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\SiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: SiteRepository::class)]
#[ApiResource(
    collectionOperations: ['get', 'post'],
    itemOperations: ['get','put','patch'],
    normalizationContext: ['groups' => ['get:Site']],
    paginationClientItemsPerPage: true,
)
]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'country' => "partial",'hutttosoft_username'=>"partial",'x3_reference'=>"partial",
    'name'=>"partial",'site_type'=>"partial",'grouping_type'=>"partial",'site_group'=>"partial",'owner_type'=>"partial"])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at'])]
#[ApiFilter(BooleanFilter::class, properties: ['is_active'=>"exact"])]
#[UniqueEntity('name')]
class Site
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:Site','get:Loan','get:Place','get:Accommodation'])]
    private $id;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['get:Site'])]
    private $is_active;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Groups(['get:Site'])]
    private $country;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Groups(['get:Site'])]
    private $hutttosoft_username;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Groups(['get:Site'])]
    private $x3_reference;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Groups(['get:Site','get:Loan','get:Place','get:Accommodation'])]
    private $name;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:Site'])]
    private $created_at;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:Site'])]
    private $updated_at;

    #[ORM\ManyToOne(targetEntity: SiteType::class, inversedBy: 'sites')]
    #[ORM\JoinColumn(name: "site_type_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Site'])]
    private $site_type;

    #[ORM\ManyToOne(targetEntity: GroupingType::class, inversedBy: 'sites')]
    #[ORM\JoinColumn(name: "grouping_type_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Site'])]
    private $grouping_type;

    #[ORM\ManyToOne(targetEntity: SiteGroup::class, inversedBy: 'sites')]
    #[ORM\JoinColumn(name: "site_group_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Site'])]
    private $site_group;

    #[ORM\ManyToOne(targetEntity: OwnerType::class, inversedBy: 'sites')]
    #[ORM\JoinColumn(name: "owner_type_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Site'])]
    private $owner_type;

    #[ORM\OneToMany(mappedBy: 'site', targetEntity: Place::class)]
    #[Groups(['get:Site'])]
    private $places;

    #[ORM\OneToMany(mappedBy: 'site', targetEntity: UserSite::class)]
    #[Groups(['get:Site'])]
    private $userSites;

    #[ORM\OneToMany(mappedBy: 'camping_source', targetEntity: Loan::class)]
    #[Groups(['get:Site'])]
    private $loans;

    public function __construct()
    {
        $this->places = new ArrayCollection();
        $this->userSites = new ArrayCollection();
        $this->loans = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getHutttosoftUsername(): ?string
    {
        return $this->hutttosoft_username;
    }

    public function setHutttosoftUsername(?string $hutttosoft_username): self
    {
        $this->hutttosoft_username = $hutttosoft_username;

        return $this;
    }

    public function getX3Reference(): ?string
    {
        return $this->x3_reference;
    }

    public function setX3Reference(?string $x3_reference): self
    {
        $this->x3_reference = $x3_reference;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getSiteType(): ?SiteType
    {
        return $this->site_type;
    }

    public function setSiteType(?SiteType $site_type): self
    {
        $this->site_type = $site_type;

        return $this;
    }

    public function getGroupingType(): ?GroupingType
    {
        return $this->grouping_type;
    }

    public function setGroupingType(?GroupingType $grouping_type): self
    {
        $this->grouping_type = $grouping_type;

        return $this;
    }

    public function getSiteGroup(): ?SiteGroup
    {
        return $this->site_group;
    }

    public function setSiteGroup(?SiteGroup $site_group): self
    {
        $this->site_group = $site_group;

        return $this;
    }

    public function getOwnerType(): ?OwnerType
    {
        return $this->owner_type;
    }

    public function setOwnerType(?OwnerType $owner_type): self
    {
        $this->owner_type = $owner_type;

        return $this;
    }

    /**
     * @return Collection<int, Place>
     */
    public function getPlaces(): Collection
    {
        return $this->places;
    }

    public function addPlace(Place $place): self
    {
        if (!$this->places->contains($place)) {
            $this->places[] = $place;
            $place->setSite($this);
        }

        return $this;
    }

    public function removePlace(Place $place): self
    {
        if ($this->places->removeElement($place)) {
            // set the owning side to null (unless already changed)
            if ($place->getSite() === $this) {
                $place->setSite(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, UserSite>
     */
    public function getUserSites(): Collection
    {
        return $this->userSites;
    }

    public function addUserSite(UserSite $userSite): self
    {
        if (!$this->userSites->contains($userSite)) {
            $this->userSites[] = $userSite;
            $userSite->setSite($this);
        }

        return $this;
    }

    public function removeUserSite(UserSite $userSite): self
    {
        if ($this->userSites->removeElement($userSite)) {
            // set the owning side to null (unless already changed)
            if ($userSite->getSite() === $this) {
                $userSite->setSite(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Loan>
     */
    public function getLoans(): Collection
    {
        return $this->loans;
    }

    public function addLoan(Loan $loan): self
    {
        if (!$this->loans->contains($loan)) {
            $this->loans[] = $loan;
            $loan->setCampingSource($this);
        }

        return $this;
    }

    public function removeLoan(Loan $loan): self
    {
        if ($this->loans->removeElement($loan)) {
            // set the owning side to null (unless already changed)
            if ($loan->getCampingSource() === $this) {
                $loan->setCampingSource(null);
            }
        }

        return $this;
    }
}
