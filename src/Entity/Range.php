<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\RangeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RangeRepository::class)]
#[ORM\Table(name: '`range`')]
#[ApiResource(
    normalizationContext: ['groups' => ['get:Range']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
    collectionOperations: ['get', 'post'],
    itemOperations: ['get','put','patch']
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'name' => "partial",'accommodation_type' => "exact"])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at'])]
#[ApiFilter(BooleanFilter::class, properties: ['is_active'])]
#[UniqueEntity('name')]
class Range
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:Range','get:Version'])]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    #[Groups(['get:Range','get:Version'])]
    private $name;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['get:Range','get:Version'])]
    private $is_active;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['get:Range'])]
    private $created_at;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['get:Range'])]
    private $updated_at;

    #[ORM\ManyToOne(targetEntity: AccommodationType::class, inversedBy: 'ranges')]
    #[ORM\JoinColumn(name: "accommodation_type_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Range'])]
    private $accommodation_type;

    #[ORM\OneToMany(mappedBy: 'range_id', targetEntity: Version::class)]
    #[Groups(['get:Range'])]
    private $versions;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
        $this->versions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getAccommodationType(): ?AccommodationType
    {
        return $this->accommodation_type;
    }

    public function setAccommodationType(?AccommodationType $accommodation_type): self
    {
        $this->accommodation_type = $accommodation_type;

        return $this;
    }

    /**
     * @return Collection<int, Version>
     */
    public function getVersions(): Collection
    {
        return $this->versions;
    }

    public function addVersion(Version $version): self
    {
        if (!$this->versions->contains($version)) {
            $this->versions[] = $version;
            $version->setRangeId($this);
        }

        return $this;
    }

    public function removeVersion(Version $version): self
    {
        if ($this->versions->removeElement($version)) {
            // set the owning side to null (unless already changed)
            if ($version->getRangeId() === $this) {
                $version->setRangeId(null);
            }
        }

        return $this;
    }
}
