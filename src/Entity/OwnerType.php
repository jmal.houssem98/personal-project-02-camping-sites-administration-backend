<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\OwnerTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: OwnerTypeRepository::class)]
#[ApiResource(
    paginationClientItemsPerPage: true,
    normalizationContext: ['groups' => ['get:OwnerType']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
    collectionOperations: ['get', 'post'],
    itemOperations: ['get','put','patch']
)
]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'name' => "partial",'sites'=>"partial"])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at'])]
#[ApiFilter(BooleanFilter::class, properties: ['is_active'=>"exact"])]
#[UniqueEntity('name')]
class OwnerType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:OwnerType','get:Site','get:Accommodation'])]
    private $id;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Groups(['get:OwnerType','get:Site','get:Accommodation'])]
    private $name;

    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['get:OwnerType'])]
    private $is_active;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:OwnerType'])]
    private $created_at;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:OwnerType'])]
    private $updated_at;

    #[ORM\OneToMany(mappedBy: 'owner_type', targetEntity: Site::class)]
    #[Groups(['get:OwnerType'])]
    private $sites;

    #[ORM\OneToMany(mappedBy: 'owner_type', targetEntity: Accommodation::class)]
    #[Groups(['get:OwnerType'])]
    private $accommodations;
//
    public function __construct()
    {
        $this->sites = new ArrayCollection();
        $this->accommodations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection<int, Site>
     */
    public function getSites(): Collection
    {
        return $this->sites;
    }

    public function addSite(Site $site): self
    {
        if (!$this->sites->contains($site)) {
            $this->sites[] = $site;
            $site->setOwnerType($this);
        }

        return $this;
    }

    public function removeSite(Site $site): self
    {
        if ($this->sites->removeElement($site)) {
            // set the owning side to null (unless already changed)
            if ($site->getOwnerType() === $this) {
                $site->setOwnerType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Accommodation>
     */
    public function getAccommodations(): Collection
    {
        return $this->accommodations;
    }

    public function addAccommodation(Accommodation $accommodation): self
    {
        if (!$this->accommodations->contains($accommodation)) {
            $this->accommodations[] = $accommodation;
            $accommodation->setOwnerType($this);
        }

        return $this;
    }

    public function removeAccommodation(Accommodation $accommodation): self
    {
        if ($this->accommodations->removeElement($accommodation)) {
            // set the owning side to null (unless already changed)
            if ($accommodation->getOwnerType() === $this) {
                $accommodation->setOwnerType(null);
            }
        }

        return $this;
    }
}
