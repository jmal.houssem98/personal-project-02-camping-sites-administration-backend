<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\AccomodationTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: AccomodationTypeRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['get:AccommodationType']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
    collectionOperations: ['get', 'post'],
    itemOperations: ['get','put','patch']
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact", 'name' => "partial",'amortization' => "exact",'guarantee' => "exact", 'rental_type' => "exact"])]
#[ApiFilter(BooleanFilter::class, properties: ['is_active'=>"exact"])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at'])]
#[ApiFilter(RangeFilter::class, properties: ['amortization','guarantee'])]

#[UniqueEntity('name')]

class AccommodationType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:AccommodationType','get:Invoice','get:Range','get:Accommodation'])]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    #[Groups(['get:AccommodationType','get:Invoice','get:Range','get:Accommodation'])]
    private $name;

    #[ORM\Column(type: 'integer')]
    #[Groups(['get:AccommodationType','get:Accommodation'])]
    private $amortization;

    #[ORM\Column(type: 'integer')]
    #[Groups(['get:AccommodationType'])]
    private $guarantee;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['get:AccommodationType','get:Invoice','get:Range'])]
    private $is_active;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['get:AccommodationType'])]
    private $created_at;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['get:AccommodationType'])]
    private $updated_at;

    #[ORM\ManyToOne(targetEntity: RentalType::class, inversedBy: 'accomodationTypes')]
    #[ORM\JoinColumn(name: "rental_type_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:AccommodationType'])]
    private $rental_type;

    #[ORM\OneToMany(mappedBy: 'accommodation_type', targetEntity: Invoice::class)]
    #[Groups(['get:AccommodationType'])]
    private $invoices;

    #[ORM\OneToMany(mappedBy: 'accommodation_type', targetEntity: Range::class)]
    #[Groups(['get:AccommodationType'])]
    private $ranges;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
        $this->invoices = new ArrayCollection();
        $this->ranges = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAmortization(): ?int
    {
        return $this->amortization;
    }

    public function setAmortization(int $amortization): self
    {
        $this->amortization = $amortization;

        return $this;
    }

    public function getGuarantee(): ?int
    {
        return $this->guarantee;
    }

    public function setGuarantee(int $guarantee): self
    {
        $this->guarantee = $guarantee;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getRentalType(): ?RentalType
    {
        return $this->rental_type;
    }

    public function setRentalType(?RentalType $rental_type): self
    {
        $this->rental_type = $rental_type;

        return $this;
    }

    /**
     * @return Collection<int, Invoice>
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->setAccommodationType($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->removeElement($invoice)) {
            // set the owning side to null (unless already changed)
            if ($invoice->getAccommodationType() === $this) {
                $invoice->setAccommodationType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Range>
     */
    public function getRanges(): Collection
    {
        return $this->ranges;
    }

    public function addRange(Range $range): self
    {
        if (!$this->ranges->contains($range)) {
            $this->ranges[] = $range;
            $range->setAccommodationType($this);
        }

        return $this;
    }

    public function removeRange(Range $range): self
    {
        if ($this->ranges->removeElement($range)) {
            // set the owning side to null (unless already changed)
            if ($range->getAccommodationType() === $this) {
                $range->setAccommodationType(null);
            }
        }

        return $this;
    }
}
