<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\FeatureRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: FeatureRepository::class)]
#[ApiResource(
    paginationClientItemsPerPage: true,
    normalizationContext: ['groups' => ['get:Feature']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
    collectionOperations: ['get', 'post'],
    itemOperations: ['get','put','patch']
)
]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'name' => "partial",'application'=>"partial"])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at'])]
#[ApiFilter(BooleanFilter::class, properties: ['is_active'=>"exact",'is_parent'=>"exact"])]
#[UniqueEntity('name')]

class Feature
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:Feature'])]
    private $id;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Groups(['get:Feature'])]
    private $name;

    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['get:Feature'])]
    private $is_active;

    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['get:Feature'])]
    private $is_parent;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Groups(['get:Feature'])]
    private $application;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:Feature'])]
    private $created_at;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['get:Feature'])]
    private $updated_at;

    #[ORM\OneToMany(mappedBy: 'feature', targetEntity: FeatureAccess::class)]
    #[Groups(['get:Feature'])]
    private $featureAccesses;

    public function __construct()
    {
        $this->featureAccesses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getIsParent(): ?bool
    {
        return $this->is_parent;
    }

    public function setIsParent(?bool $is_parent): self
    {
        $this->is_parent = $is_parent;

        return $this;
    }

    public function getApplication(): ?string
    {
        return $this->application;
    }

    public function setApplication(?string $application): self
    {
        $this->application = $application;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection<int, FeatureAccess>
     */
    public function getFeatureAccesses(): Collection
    {
        return $this->featureAccesses;
    }

    public function addFeatureAccess(FeatureAccess $featureAccess): self
    {
        if (!$this->featureAccesses->contains($featureAccess)) {
            $this->featureAccesses[] = $featureAccess;
            $featureAccess->setFeature($this);
        }

        return $this;
    }

    public function removeFeatureAccess(FeatureAccess $featureAccess): self
    {
        if ($this->featureAccesses->removeElement($featureAccess)) {
            // set the owning side to null (unless already changed)
            if ($featureAccess->getFeature() === $this) {
                $featureAccess->setFeature(null);
            }
        }

        return $this;
    }
}
