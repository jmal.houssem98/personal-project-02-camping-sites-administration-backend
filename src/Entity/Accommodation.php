<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\AccommodationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: AccommodationRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['get:Accommodation']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,

)]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'leasing' => "partial",'provider_bill_number'=>'partial','installation_type' => "exact",'version' => "exact",'place' => "exact"])]
#[ApiFilter(DateFilter::class, properties: ['created_at','updated_at','removing_date','installation_date','commissioning_date'])]
#[ApiFilter(RangeFilter::class, properties: ['value','annual_rent'])]
##[UniqueEntity('version')]


class Accommodation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:Accommodation','get:Element','get:Loan'])]
    private $id;

    #[ORM\Column(type: 'date')]
    #[Groups(['get:Accommodation'])]
    private $commissioning_date;

    #[ORM\Column(type: 'string', length: 255, nullable:true)]
    #[Groups(['get:Accommodation'])]
    private $comment;

    #[ORM\Column(type: 'integer')]
    #[Groups(['get:Accommodation'])]
    private $value;

    #[ORM\Column(type: 'integer')]
    #[Groups(['get:Accommodation'])]
    private $annual_rent;

    #[ORM\Column(type: 'string', length: 100, nullable:true)]
    #[Groups(['get:Accommodation'])]
    private $provider_bill_number;

    #[ORM\Column(type: 'string', length: 100)]
    #[Groups(['get:Accommodation','get:Element'])]
    private $leasing;

    #[ORM\Column(type: 'date', nullable:true)]
    #[Groups(['get:Accommodation'])]
    private $removing_date;

    #[ORM\Column(type: 'date', nullable:true)]
    #[Groups(['get:Accommodation'])]
    private $installation_date;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['get:Accommodation'])]
    private $created_at;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['get:Accommodation'])]
    private $updated_at;

    #[ORM\ManyToOne(targetEntity: InstallationType::class, inversedBy: 'accommodations')]
    #[ORM\JoinColumn(name: "installation_type_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Accommodation'])]
    private $installation_type;

    #[ORM\ManyToOne(targetEntity: Version::class, inversedBy: 'accommodations')]
    #[ORM\JoinColumn(name: "version_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Accommodation','get:Element'])]
    private $version;

    #[ORM\OneToMany(mappedBy: 'accommodation', targetEntity: Element::class)]
    #[Groups(['get:Accommodation'])]
    private $elements;

    #[ORM\OneToMany(mappedBy: 'accommodation_source', targetEntity: Loan::class)]
    #[Groups(['get:Accommodation'])]
    private $loans;

    #[ORM\ManyToOne(targetEntity: OwnerType::class, inversedBy: 'accommodations')]
    #[ORM\JoinColumn(name: "owner_type_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Accommodation'])]
    private $owner_type;

    #[ORM\ManyToOne(targetEntity: Place::class, inversedBy: 'accommodations')]
    #[ORM\JoinColumn(name: "place_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Accommodation'])]
    private $place;

    #[ORM\ManyToOne(targetEntity: AccommodationState::class, inversedBy: 'accommodations')]
    #[ORM\JoinColumn(name: "state_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Accommodation'])]
    private $state;

    #[ORM\OneToMany(mappedBy: 'accommodation', targetEntity: AccommodationHistory::class)]
    private $accommodationHistories;

    public function __construct(){
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
        $this->elements = new ArrayCollection();
        $this->loans = new ArrayCollection();
        $this->accommodationHistories = new ArrayCollection();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCommissioningDate(): ?\DateTimeInterface
    {
        return $this->commissioning_date;
    }

    public function setCommissioningDate(\DateTimeInterface $commissioning_date): self
    {
        $this->commissioning_date = $commissioning_date;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getAnnualRent(): ?int
    {
        return $this->annual_rent;
    }

    public function setAnnualRent(int $annual_rent): self
    {
        $this->annual_rent = $annual_rent;

        return $this;
    }

    public function getProviderBillNumber(): ?string
    {
        return $this->provider_bill_number;
    }

    public function setProviderBillNumber(string $provider_bill_number): self
    {
        $this->provider_bill_number = $provider_bill_number;

        return $this;
    }

    public function getLeasing(): ?string
    {
        return $this->leasing;
    }

    public function setLeasing(string $leasing): self
    {
        $this->leasing = $leasing;

        return $this;
    }

    public function getRemovingDate(): ?\DateTimeInterface
    {
        return $this->removing_date;
    }

    public function setRemovingDate(\DateTimeInterface $removing_date): self
    {
        $this->removing_date = $removing_date;

        return $this;
    }

    public function getInstallationDate(): ?\DateTimeInterface
    {
        return $this->installation_date;
    }

    public function setInstallationDate(\DateTimeInterface $installation_date): self
    {
        $this->installation_date = $installation_date;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getInstallationType(): ?InstallationType
    {
        return $this->installation_type;
    }

    public function setInstallationType(?InstallationType $installation_type): self
    {
        $this->installation_type = $installation_type;

        return $this;
    }

    public function getVersion(): ?Version
    {
        return $this->version;
    }

    public function setVersion(?Version $version): self
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @return Collection<int, Element>
     */
    public function getElements(): Collection
    {
        return $this->elements;
    }

    public function addElement(Element $element): self
    {
        if (!$this->elements->contains($element)) {
            $this->elements[] = $element;
            $element->setAccommodation($this);
        }

        return $this;
    }

    public function removeElement(Element $element): self
    {
        if ($this->elements->removeElement($element)) {
            // set the owning side to null (unless already changed)
            if ($element->getAccommodation() === $this) {
                $element->setAccommodation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Loan>
     */
    public function getLoans(): Collection
    {
        return $this->loans;
    }

    public function addLoan(Loan $loan): self
    {
        if (!$this->loans->contains($loan)) {
            $this->loans[] = $loan;
            $loan->setAccommodationSource($this);
        }

        return $this;
    }

    public function removeLoan(Loan $loan): self
    {
        if ($this->loans->removeElement($loan)) {
            // set the owning side to null (unless already changed)
            if ($loan->getAccommodationSource() === $this) {
                $loan->setAccommodationSource(null);
            }
        }

        return $this;
    }

    public function getOwnerType(): ?OwnerType
    {
        return $this->owner_type;
    }

    public function setOwnerType(?OwnerType $owner_type): self
    {
        $this->owner_type = $owner_type;

        return $this;
    }

    public function getPlace(): ?Place
    {
        return $this->place;
    }

    public function setPlace(?Place $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getState(): ?AccommodationState
    {
        return $this->state;
    }

    public function setState(?AccommodationState $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return Collection<int, AccommodationHistory>
     */
    public function getAccommodationHistories(): Collection
    {
        return $this->accommodationHistories;
    }

    public function addAccommodationHistory(AccommodationHistory $accommodationHistory): self
    {
        if (!$this->accommodationHistories->contains($accommodationHistory)) {
            $this->accommodationHistories[] = $accommodationHistory;
            $accommodationHistory->setAccommodation($this);
        }

        return $this;
    }

    public function removeAccommodationHistory(AccommodationHistory $accommodationHistory): self
    {
        if ($this->accommodationHistories->removeElement($accommodationHistory)) {
            // set the owning side to null (unless already changed)
            if ($accommodationHistory->getAccommodation() === $this) {
                $accommodationHistory->setAccommodation(null);
            }
        }

        return $this;
    }
}
