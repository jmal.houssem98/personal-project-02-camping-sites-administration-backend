<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ElementTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ElementTypeRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['get:ElementType']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'version' => "exact",'name'=>'partial'])]
#[ApiFilter(BooleanFilter::class, properties: ['mandatory'])]
#[UniqueEntity('name')]
class ElementType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:ElementType','get:Pricing','get:Element','get:Accommodation','get:Version'])]
    private $id;

    #[ORM\Column(type: 'string', length: 100)]
    #[Groups(['get:ElementType','get:Pricing','get:Element','get:Accommodation','get:Version'])]
    private $name;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['get:ElementType'])]
    private $mandatory;

    #[ORM\ManyToOne(targetEntity: Version::class, inversedBy: 'elementTypes')]
    #[ORM\JoinColumn(name: "version_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:ElementType'])]
    private $version;

    #[ORM\OneToMany(mappedBy: 'element_type', targetEntity: Pricing::class)]
    #[Groups(['get:ElementType'])]
    private $pricings;

    #[ORM\OneToMany(mappedBy: 'element_type', targetEntity: Element::class)]
    #[Groups(['get:ElementType'])]
    private $elements;

    public function __construct()
    {
        $this->pricings = new ArrayCollection();
        $this->elements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMandatory(): ?bool
    {
        return $this->mandatory;
    }

    public function setMandatory(bool $mandatory): self
    {
        $this->mandatory = $mandatory;

        return $this;
    }

    public function getVersion(): ?Version
    {
        return $this->version;
    }

    public function setVersion(?Version $version): self
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @return Collection<int, Pricing>
     */
    public function getPricings(): Collection
    {
        return $this->pricings;
    }

    public function addPricing(Pricing $pricing): self
    {
        if (!$this->pricings->contains($pricing)) {
            $this->pricings[] = $pricing;
            $pricing->setElementType($this);
        }

        return $this;
    }

    public function removePricing(Pricing $pricing): self
    {
        if ($this->pricings->removeElement($pricing)) {
            // set the owning side to null (unless already changed)
            if ($pricing->getElementType() === $this) {
                $pricing->setElementType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Element>
     */
    public function getElements(): Collection
    {
        return $this->elements;
    }

    public function addElement(Element $element): self
    {
        if (!$this->elements->contains($element)) {
            $this->elements[] = $element;
            $element->setElementType($this);
        }

        return $this;
    }

    public function removeElement(Element $element): self
    {
        if ($this->elements->removeElement($element)) {
            // set the owning side to null (unless already changed)
            if ($element->getElementType() === $this) {
                $element->setElementType(null);
            }
        }

        return $this;
    }
}
