<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\LoanRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: LoanRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['get:Loan']],
    paginationItemsPerPage: 10,
    paginationMaximumItemsPerPage: 10,
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => "exact",'accommodation_source' => "exact",'element' => "exact",'accommodation_target' => "exact"
    ,'camping_source' => "exact",'camping_target' => "exact"])]
#[ApiFilter(DateFilter::class, properties: ['end_date','start_date'])]
class Loan
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['get:Loan','get:Element'])]
    private $id;

    #[ORM\Column(type: 'date')]
    #[Groups(['get:Loan'])]
    private $start_date;

    #[ORM\Column(type: 'date')]
    #[Groups(['get:Loan'])]
    private $end_date;

    #[ORM\ManyToOne(targetEntity: Element::class, inversedBy: 'loans')]
    #[ORM\JoinColumn(name: "element_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Loan'])]
    private $element;

    #[ORM\ManyToOne(targetEntity: Accommodation::class, inversedBy: 'loans')]
    #[ORM\JoinColumn(name: "accommodation_source_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Loan',])]
    private $accommodation_source;

    #[ORM\ManyToOne(targetEntity: Accommodation::class, inversedBy: 'loans')]
    #[ORM\JoinColumn(name: "accommodation_target_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Loan'])]
    private $accommodation_target;

    #[ORM\ManyToOne(targetEntity: Site::class, inversedBy: 'loans')]
    #[ORM\JoinColumn(name: "camping_source_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Loan'])]
    private $camping_source;

    #[ORM\ManyToOne(targetEntity: Site::class, inversedBy: 'loans')]
    #[ORM\JoinColumn(name: "camping_target_id",referencedColumnName:"id", nullable:true, onDelete:"SET NULL")]
    #[Groups(['get:Loan'])]
    private $camping_target;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->start_date;
    }

    public function setStartDate(\DateTimeInterface $start_date): self
    {
        $this->start_date = $start_date;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->end_date;
    }

    public function setEndDate(\DateTimeInterface $end_date): self
    {
        $this->end_date = $end_date;

        return $this;
    }

    public function getElement(): ?Element
    {
        return $this->element;
    }

    public function setElement(?Element $element): self
    {
        $this->element = $element;

        return $this;
    }

    public function getAccommodationSource(): ?Accommodation
    {
        return $this->accommodation_source;
    }

    public function setAccommodationSource(?Accommodation $accommodation_source): self
    {
        $this->accommodation_source = $accommodation_source;

        return $this;
    }

    public function getAccommodationTarget(): ?Accommodation
    {
        return $this->accommodation_target;
    }

    public function setAccommodationTarget(?Accommodation $accommodation_target): self
    {
        $this->accommodation_target = $accommodation_target;

        return $this;
    }

    public function getCampingSource(): ?Site
    {
        return $this->camping_source;
    }

    public function setCampingSource(?Site $camping_source): self
    {
        $this->camping_source = $camping_source;

        return $this;
    }

    public function getCampingTarget(): ?Site
    {
        return $this->camping_target;
    }

    public function setCampingTarget(?Site $camping_target): self
    {
        $this->camping_target = $camping_target;

        return $this;
    }
}
