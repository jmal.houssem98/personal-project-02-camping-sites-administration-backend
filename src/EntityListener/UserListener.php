<?php

namespace App\EntityListener;

use App\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserListener
{
    private MailerInterface $mailer;
    private UserPasswordHasherInterface $harsher;
    private $logger;

    public function __construct(MailerInterface $mailer, UserPasswordHasherInterface $hashed, LoggerInterface $logger)
    {
        $this->mailer = $mailer;
        $this->harsher = $hashed;
        $this->logger = $logger;
    }

    public function prePersist(User $user)
    {
        $this->send($user);
        $this->encodePassword($user);
    }

    public function preUpdate(User $user)
    {
        $this->encodePassword($user);
    }

    /**
     * Encode password based on plain password
     *
     * @param User $user
     * @return void
     */
    public function encodePassword(User $user): void
    {
        if ($user->getPassword() === null ||strlen($user->getPassword())>40 ) {
            return;
        }

        $user->setPassword(
            $this->harsher->hashPassword(
                $user,
                $user->getPassword()
            )
        );
    }
    public function send(user $user): void
    {
        $email = (new Email())
            ->from('hello@example.com')
            ->to($user->getEmail())
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject('Time for Symfony Mailer!')
            ->text($user->getEmail(),$user->getPassword())
            ->html('Bonjour '.$user->getFirstName().' '.$user->getLastName().'     
              Veuillez trouver ci-joint les informations nécessaires pour vous connecter à l Espace Client.'.'
                - Identifiant :'.$user->getEmail().' 
                - Mot de passe : '.$user->getPassword());
        $this->mailer->send($email);
    }
}
