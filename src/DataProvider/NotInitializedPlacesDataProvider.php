<?php

declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Place;
use App\Repository\PlaceRepository;
use Exception;

/**
 * Called by /places/not_initialized?siteId={siteId}&versionId={versionId}.
 */
class NotInitializedPlacesDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    private PlaceRepository $placeRepository;

    public function __construct(PlaceRepository $placeRepository)
    {
        $this->placeRepository = $placeRepository;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Place::class === $resourceClass && 'getNotInitializedPlaces' === $context['collection_operation_name'];
    }

    /**
     * @throws Exception
     *
     * @return Place[]
     */
    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): array
    {
        $filters = $context['filters'] ?? [];
        $orders = $context['filters']['order'] ?? [];

        return $this->placeRepository->getNotInitializedPlaces($filters, $orders);
    }
}
