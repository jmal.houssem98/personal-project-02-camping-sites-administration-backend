<?php

declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Version;
use App\Repository\VersionRepository;
use Exception;

/**
 * Called by /versions/not_initialized?siteId={siteId}.
 */
class NotInitializedVersionsDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    private VersionRepository $versionRepository;

    public function __construct(VersionRepository $versionRepository)
    {
        $this->versionRepository = $versionRepository;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Version::class === $resourceClass && 'getNotInitializedVersions' === $context['collection_operation_name'];
    }

    /**
     * @throws Exception
     *
     * @return Version[]
     */
    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): array
    {
        $filters = $context['filters'] ?? [];
        $orders = $context['filters']['order'] ?? [];

        return $this->versionRepository->getNotInitializedVersions($filters, $orders);
    }
}
