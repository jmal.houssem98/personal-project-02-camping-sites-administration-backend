<?php

namespace App\EventListner;
use App\Entity\User;
use Symfony\Component\HttpFoundation\RequestStack;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\Security\Core\User\UserInterface;

class JWTCreatedListnee
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();

        if (!$user instanceof UserInterface) {
            return;
        }
        if ($user instanceof User) {
            $data['user'] = array(
                'id'        => $user->getId(),
                'email'     => $user->getEmail(),
                'roles'     => $user->getRoles(),
                "profile" => $user->getProfile(),
                 "userSites"=> $user->getUserSites(),
                "reportingFiles"=> $user->getReportingFiles(),
                 "accommodationHistories"=> $user->getAccommodationHistories(),
                "elementHistories"=> $user->getElementHistories(),
                "username"=> $user->getUserIdentifier(),
                 "last_name"=> $user->getLastName(),
                 "first_name"=> $user->getFirstName(),
                 "is_active"=> $user->getIsActive(),
                 "entry_date"=> $user->getEntryDate(),
                 "end_date"=> $user->getEndDate(),
                 "language"=> $user->getLanguage(),
                 "account_confirmation_token"=> $user->getAccountConfirmationToken(),
                  "account_reset_password_token"=> $user->getAccountResetPasswordToken(),
                 "confirmed_account"=> $user->getConfirmedAccount(),
                 "created_at"=> $user->getCreatedAt(),
                 "updated_at"=> $user->getUpdatedAt(),
            );
        }
        $event->setData($data);
    }
}