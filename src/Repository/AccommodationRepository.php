<?php

namespace App\Repository;

use App\Entity\Accommodation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Accommodation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Accommodation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Accommodation[]    findAll()
 * @method Accommodation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccommodationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Accommodation::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Accommodation $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Accommodation $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
    public function findNotYetInitialized(int $siteId, bool $onlyIds = true): array
    {
        $queryBuilder = $this->createQueryBuilder('accommodation');

        $query = $queryBuilder
            ->distinct(true)
            ->innerJoin('accommodation.elements', 'elements', Join::ON)
            ->innerJoin('accommodation.place', 'place', Join::ON)
            ->innerJoin('place.site', 'site', Join::ON)
            ->innerJoin('elements.element_type', 'element_type', Join::ON)
            ->where($queryBuilder->expr()->eq('site.id', $siteId))
            ->andWhere('element_type.mandatory = :mandatory')
            ->setParameter('mandatory', 1)
            ->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->isNull('elements.recognition_code'),
                    $queryBuilder->expr()->eq('elements.recognition_code', "''")
                )
            )
            ->getQuery();

        $result = $query->getResult();

        if ($onlyIds) {
            /** @var int[] $returnedValues */
            $returnedValues = array_map(
                static function (Accommodation $accommodation) {
                    return $accommodation->getId();
                }, $result
            );

            return !empty($returnedValues) ? $returnedValues : [0];
        }

        return $result;
    }

    // /**
    //  * @return Accommodation[] Returns an array of Accommodation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Accommodation
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
