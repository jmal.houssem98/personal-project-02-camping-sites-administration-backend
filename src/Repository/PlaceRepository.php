<?php

namespace App\Repository;

use App\Entity\Accommodation;
use App\Entity\Place;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * @method Place|null find($id, $lockMode = null, $lockVersion = null)
 * @method Place|null findOneBy(array $criteria, array $orderBy = null)
 * @method Place[]    findAll()
 * @method Place[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Place::class);
        /** @var AccommodationRepository $accommodationRepository */
        $accommodationRepository = $registry->getRepository(Accommodation::class);

        $this->accommodationRepository = $accommodationRepository;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Place $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Place $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
    public const GET_NOT_INITIALIZED_PLACES_FIELDS_MAP = [
        'siteId' => 'site.id',
        'versionId' => 'version.id',
    ];

    private AccommodationRepository $accommodationRepository;


    public function findOneByPlaceAndSite(string $placeName, string $siteName): ?Place
    {
        $queryBuilder = $this->createQueryBuilder('place');

        $places = $queryBuilder
            ->innerJoin('place.site', 'site', Join::ON)
            ->where($queryBuilder->expr()->like('site.name', "'{$siteName}'"))
            ->andWhere($queryBuilder->expr()->like('place.name', "'{$placeName}'"))
            ->getQuery()
            ->getResult();

        return count($places) >= 1 ? reset($places) : null;
    }

    /**
     * @throws Exception
     *
     * @return Place[]
     */
    public function getNotInitializedPlaces(array $filters, array $orders, bool $onlyNotYetInitialized = true): array
    {
        /** @var int $siteIdKey */
        $siteIdKey = array_search('site.id', self::GET_NOT_INITIALIZED_PLACES_FIELDS_MAP, true);
        $siteId = (int) $filters[$siteIdKey];

        /** @var int $versionIdKey */
        $versionIdKey = array_search('version.id', self::GET_NOT_INITIALIZED_PLACES_FIELDS_MAP, true);
        $versionId = $filters[$versionIdKey] ?? null;

        $queryBuilder = $this->createQueryBuilder('place');
        $queryBuilder
            ->innerJoin('place.site', 'site', Join::ON)
            ->innerJoin('place.accommodations', 'accommodations', Join::ON);

        if (null !== $versionId) {
            $queryBuilder
                ->innerJoin('accommodations.version', 'version', Join::ON);
        }

        $queryBuilder
            ->groupBy('place.id');

        $page = (int) ($filters['page'] ?? 1);
        $limit = isset($filters['itemsPerPage']) ? (int) $filters['itemsPerPage'] : null;

        if (null !== $limit) {
            $queryBuilder->setMaxResults($limit);
            $queryBuilder->setFirstResult(RepositoryHelper::getOffset($page, $limit));
        }

        if (
            (true === $onlyNotYetInitialized)
            && ([] !== ($notYetInitializedAccommodationsIds = $this->accommodationRepository->findNotYetInitialized($siteId)))
        ) {
            $queryBuilder->andWhere($queryBuilder->expr()->in('accommodations.id', $notYetInitializedAccommodationsIds));
        }

        $queryBuilder = RepositoryHelper::applyFilters($queryBuilder, $filters, self::GET_NOT_INITIALIZED_PLACES_FIELDS_MAP);
        $queryBuilder = RepositoryHelper::applyOrders($queryBuilder, $orders, self::GET_NOT_INITIALIZED_PLACES_FIELDS_MAP);

        return $queryBuilder->getQuery()->getResult();
    }

    // /**
    //  * @return Place[] Returns an array of Place objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Place
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
