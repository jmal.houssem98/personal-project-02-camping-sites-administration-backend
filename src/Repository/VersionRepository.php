<?php

namespace App\Repository;

use App\Entity\Accommodation;
use App\Entity\Version;
use App\Helper\RepositoryHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\Expr\Join;
use Exception;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Version|null find($id, $lockMode = null, $lockVersion = null)
 * @method Version|null findOneBy(array $criteria, array $orderBy = null)
 * @method Version[]    findAll()
 * @method Version[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VersionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Version::class);

        /** @var AccommodationRepository $accommodationRepository */
        $accommodationRepository = $registry->getRepository(Accommodation::class);

        $this->accommodationRepository = $accommodationRepository;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Version $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Version $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public const GET_NOT_INITIALIZED_VERSIONS_FIELDS_MAP = [
        'siteId' => 'site.id',
    ];

    private AccommodationRepository $accommodationRepository;

    /**
     * @param mixed $versionName
     * @param mixed $accommodationTypeName
     *
     * @throws NonUniqueResultException
     */
    public function findOneByNameAndAccommodationType($versionName, $accommodationTypeName): ?Version
    {
        $queryBuilder = $this->createQueryBuilder('version');
        $queryBuilder
            ->innerJoin('version.range', 'range', Join::ON)
            ->innerJoin('range.accommodationType', 'accommodationType', Join::ON)
            ->where($queryBuilder->expr()->like('accommodationType.name', "'{$accommodationTypeName}'"))
            ->andWhere($queryBuilder->expr()->like('version.name', "'{$versionName}'"));

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * @throws Exception
     *
     * @return Version[]
     */
    public function getNotInitializedVersions(array $filters, array $orders, bool $onlyNotYetInitialized = true): array
    {
        /** @var int $siteIdKey */
        $siteIdKey = array_search('site.id', self::GET_NOT_INITIALIZED_VERSIONS_FIELDS_MAP, true);
        $siteId = (int) $filters[$siteIdKey];

        $queryBuilder = $this->createQueryBuilder('version');
        $queryBuilder
            ->innerJoin('version.accommodations', 'accommodations', Join::ON)
            ->innerJoin('accommodations.place', 'place', Join::ON)
            ->innerJoin('place.site', 'site', Join::ON)
            ->groupBy('version.id');

        $page = (int) ($filters['page'] ?? 1);
        $limit = isset($filters['itemsPerPage']) ? (int) $filters['itemsPerPage'] : null;

        if (null !== $limit) {
            $queryBuilder->setMaxResults($limit);
            $queryBuilder->setFirstResult(RepositoryHelper::getOffset($page, $limit));
        }

        if (
            (true === $onlyNotYetInitialized)
            && ([] !== ($notYetInitializedAccommodationsIds = $this->accommodationRepository->findNotYetInitialized($siteId)))
        ) {
            $queryBuilder->andWhere($queryBuilder->expr()->in('accommodations.id', $notYetInitializedAccommodationsIds));
        }

        $queryBuilder = RepositoryHelper::applyFilters($queryBuilder, $filters, self::GET_NOT_INITIALIZED_VERSIONS_FIELDS_MAP);
        $queryBuilder = RepositoryHelper::applyOrders($queryBuilder, $orders, self::GET_NOT_INITIALIZED_VERSIONS_FIELDS_MAP);

        return $queryBuilder->getQuery()->getResult();
    }

    // /**
    //  * @return Version[] Returns an array of Version objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Version
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
