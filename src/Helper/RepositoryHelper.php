<?php

namespace App\Helper;

use Doctrine\ORM\QueryBuilder;
use Exception;

class RepositoryHelper
{
    /**
     * @throws Exception
     */
    public static function applyFilters(QueryBuilder $queryBuilder, array $filters, array $fieldsMap): QueryBuilder
    {
        /** @var string $filter */
        foreach ($filters as $filter => $value) {
            if (!isset($fieldsMap[$filter])) {
                continue;
            }

            if (
                'id' === $filter
                || strpos($filter, 'Id') === strlen($filter) - strlen('Id')
            ) {
                $comparison = $queryBuilder->expr()->eq($fieldsMap[$filter], (int) $value);
            } elseif (strpos($filter, 'Date') === strlen($filter) - strlen('Date')) {
                $comparison = $queryBuilder->expr()->eq($fieldsMap[$filter], "'{$value}'");
            } else {
                $comparison = $queryBuilder->expr()->like('LOWER(' . $fieldsMap[$filter] . ')', $queryBuilder->expr()->literal("%{$value}%"));
            }
            $queryBuilder = $queryBuilder->andWhere($comparison);
        }

        return $queryBuilder;
    }

    public static function applyOrders(QueryBuilder $queryBuilder, array $orders, array $fieldsMap): QueryBuilder
    {
        foreach ($orders as $order => $direction) {
            if (!isset($fieldsMap[$order])) {
                return $queryBuilder;
            }

            $queryBuilder = $queryBuilder->addOrderBy($fieldsMap[$order], $direction);
        }

        return $queryBuilder;
    }

    public static function getOffset(int $page = 1, ?int $limit = null): int
    {
        return 1 < $page && null !== $limit ? $limit * ($page - 1) : 0;
    }
}
